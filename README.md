# GirlsCodeLincoln - JuiceShop
*Instructions by Cole Nelson, Eric Zhou, Christopher King, and Anthony Boone of Nelnet. Adapted from resources made possbile by [OWASP JuiceShop](https://owasp.org/www-project-juice-shop/) and [Solution Manual by bkimminich](https://bkimminich.gitbooks.io/pwning-owasp-juice-shop/content/appendix/solutions.html)*.

## About
[OWASP JuiceShop](https://owasp.org/www-project-juice-shop/) is an intentionally insecure web application designed to teach common security vulnerabilities and exploits. The difficulties range from 1-star (easy) to 5-star (difficult). For this exercise, we will focus on two 1-star and one 2-star exploits to teach the basics of cybersecurity.

## Lesson Time
1-2 Hours

## Prerequisites

 1. Introductory computer and programming skills
 
 2. Access to a JuiceShop instance (see setup)

## Objectives
By the end of this lesson, students will be able to...

 1. Understand the ethics, implications, and outcomes of hacking.
 
 2. Understand the basic workings of a web application.
 
 3. Understand the [OWASP Top 10 Web Application Security Risks](https://owasp.org/www-project-top-ten/).
 
 4. Be able to apply (from the OWASP Top 10)...
 
    1. [A6:2017-Security Misconfiguration](https://owasp.org/www-project-top-ten/2017/A6_2017-Security_Misconfiguration) by performing a directory browsing attack.
	
    2. [A7:2017-Cross-Site Scripting XSS](https://owasp.org/www-project-top-ten/2017/A7_2017-Cross-Site_Scripting_(XSS)) by performing an XSS attack.
	
    3. [A1:2017-Injection](https://owasp.org/www-project-top-ten/2017/A1_2017-Injection) and [A2:2017-Broken Authentication](https://owasp.org/www-project-top-ten/2017/A2_2017-Broken_Authentication) by performing a SQL injection.
    

## Setup
**IMPORTANT**: Perform this setup in advance of students arrival. Perform a trial run of the exploits listed below so that you have a thorough understanding of how they work. The exploits are non-state-changing, so multiple people can repeat them on the same instance.

We recommend two ways of setting up JuiceShop...

 1. Run a local Docker instance of JuiceShop
 
 2. Use Heroku to host an instance of JuiceShop

See further details [here](https://hub.docker.com/r/bkimminich/juice-shop) and as explained below.

### Docker Setup
If students have access to download and install [Docker](https://www.docker.com/), they can run a local instance of JuiceShop on their own machine. Simply execute...


```bash
docker pull bkimminich/juice-shop
docker run --rm -p 3000:3000 bkimminich/juice-shop
```

Then, navigate to `http://localhost:3000` (or `http://192.168.99.100:3000/` on older machines) to get started!

### Heroku Setup
Students can share an instance of JuiceShop through a free $0/month dyno instance on Heroku. Sign up for an account and [click here](https://heroku.com/deploy) to deploy the application.

## Lesson Execution

### Introduction - Cybersecurity
Go over the ethics of hacking with students. Explain the difference between [white, grey, and black hat hacking](https://www.tripwire.com/state-of-security/security-data-protection/white-hat-black-hat-and-grey-hat-hackers-difference/). Explain that, what you are doing today is *only allowed* because you have the explicit consent of the site owner (you).

Go over [recent news concerning cybersecurity](https://www.google.com/search?q=cybersecurity+incidents&tbm=nws) (there's always something new!) Explain the motivations, costs, and consequences of these attacks. Explore the perspectives and goals of both the attacker and defender. If time allows, explore the different roles of the [red and blue team](https://purplesec.us/red-team-vs-blue-team-cyber-security/) (today we will be on the *red* team!)

### Introduction - Web Applications
If students aren't already familiar, introduce the basic concepts of web applications. Show that most applications consist of...

 - HTML - The *layout* of the web page.
 
 - CSS - The *styling* of the web page.
 
 - JavaScript - The *functionality* of the webpage.

... which are all just **text**! On JuiceShop (or any webpage), view the network log and page source to show students what this looks like. Show how the HTML tags layout the elements, the CSS stylesheet formats the display, and the JavaScript is code that moves things around!

**Don't get too deep in the details!** Keep the explanations high-level. Students will only need a *basic* understanding of web applications to do the attacks we will be performing.

Take a break for questions. In the next step, we will be performing the 3 exploits.

### Connect to JuiceShop
Now that the concepts have been introduced, we can start hacking. Have students connect to JuiceShop, (`http://localhost:3000`, `http://192.168.99.100:3000/`, or the IP given by Heroku based on your setup process) and  browse the site. Give them five minutes to get a feel for the website and see if they can find anything on their own.

We will refer to `localhost:3000`, `192.168.99.100:3000`, or the Heroku IP as `BASE_URL`.

### Exploits
For further explanations on the solutions, visit [the solution manual by bkimminich](https://bkimminich.gitbooks.io/pwning-owasp-juice-shop/content/appendix/solutions.html).

#### A6:2017-Security Misconfiguration (1-star)
After students are comfortable with using the website, have them navigate to the "legal" terms page (`http://localhost:3000/ftp/legal.md`). Ask if they can find any way to find information that they shouldn't be able to access. Students will likely scour the legal terms looking for any hints or clues. After a minute, give them a hint and have them look at the URL.

It's currently `http://BASE_URL/ftp/legal.md`. What do we think will happen if we go to `http://BASE_URL/ftp`? (note: FTP stands for 'file transfer protocol').

Explore the directory -- there are a lot of files you shouldn't be able to see. This is a security risk! It's now time to bring up the [OWASP Top 10 Web Application Security Risks](https://owasp.org/www-project-top-ten/). Briefly go over each and ask students which they believe this exploit falls under. While there may be many good answers, it likely best fits under [A6:2017-Security Misconfiguration](https://owasp.org/www-project-top-ten/2017/A6_2017-Security_Misconfiguration).

#### A7:2017-Cross-Site Scripting XSS (1-star)
Have students return back to the homepage. Tell them that there is an exploit with the search bar, and have them explore for a minute or two.

After some time, remind students about what they learned about web pages. Ask what the three parts to each web page is (HTML, CSS, JavaScript). When they search "banana," what happens? Show that the text "banana" appears *embedded* in the webpage (use the word *embedded*). What would happen if we searched for some HTML code, say `<h1>banana</h1>`. This will be *embedded* in the search, too.

Finally, what happens if we search for/embed *HTML* and *JavaScript* code? Try `<iframe src="javascript:alert('hello world!')">`. There are many more malicious things that can be done, and this can all be sent to someone via the URL! Again, have students visit the OWASP Top 10 and identify this as [A7:2017-Cross-Site Scripting XSS](https://owasp.org/www-project-top-ten/2017/A7_2017-Cross-Site_Scripting_(XSS)).

#### A1:2017-Injection and A2:2017-Broken Authentication
The last attack we will do is the most common and malicious attack (as defined by OWASP), SQL Injection! Have the students navigate to the login page and tell them to try to login as the user 'admin@juice-sh.op'. After a minute or two, ask students what they tried.

Hopefully someone has tried combinations such as username: 'admin' password: 'admin', or username: 'admin' password: 'password'. Congratulate students on trying exploit [A2:2017-Broken Authentication](https://owasp.org/www-project-top-ten/2017/A2_2017-Broken_Authentication), using default credentials. The true password was admin123.

**However**, this is not the exploit we will be using for this problem. We will instead be performing a [SQL injection](https://www.w3schools.com/sql/sql_injection.asp), which sends bad data to the database.

**Do not attempt to explain SQL here, keep it high-level**. Encourage students to learn what a SQL injection is on their own, and come back next time with why it works.

On the login page, use the following credentials...


```
email: ' or 1=1--
password: anything
```

Press login and you're in! Why does this work? We gave the database some "bad data" that it tried to interpret, but couldn't. Remember -- all programs, algorithms, and statements are just text!

Discuss the severity with students. Why is this the #1 exploit? What can you do now that you're admin? Leave the rest of the time for students to explore the site as admin, and encourage them to try to find other exploits on their own. Again, you can reference [the solution manual by bkimminich](https://bkimminich.gitbooks.io/pwning-owasp-juice-shop/content/appendix/solutions.html).

### Conclusion
With ~5-10 minutes remaining, recap what you learned today.

 1. Have students explain the ethics, implications, and outcomes of hacking.
 
 2. Have students explain the basic workings of a web application.
 
 3. Have students explain the [OWASP Top 10 Web Application Security Risks](https://owasp.org/www-project-top-ten/), and the exploits they performed today.

Remind students that these are *not* exploits that they should try at home, and have legal consequences. As a takeaway, have students (1) learn more about SQL injections and other vulnerabilities and/or (2) explore the measures that can be taken to *prevent* these attacks (circle back to [red vs blue team](https://purplesec.us/red-team-vs-blue-team-cyber-security/)).

### Wrapping Up
Shut down any docker instances or heroku app hosting the application. Remember, this is a *vulnerable* web application :)
